package testgfi.bessugesv.fr.testgofindit.model;

/**
 * Data container for an "Explore" card.<br/<br/>
 *
 *
 * Created by Vincent Bessuges on 27/12/2014.
 */
public class ExploreItem {

    private int mBackgroundResId;
    private String mTitle;
    private String mQuote;
    private String mAuthor;

    public int getBackgroundResId() {
        return mBackgroundResId;
    }

    public void setBackgroundResId(int backgroundResId) {
        mBackgroundResId = backgroundResId;
    }

    public String getTitle() {
        return mTitle;
    }

    public void setTitle(String title) {
        mTitle = title;
    }

    public String getQuote() {
        return mQuote;
    }

    public void setQuote(String quote) {
        mQuote = quote;
    }

    public String getAuthor() {
        return mAuthor;
    }

    public void setAuthor(String author) {
        mAuthor = author;
    }

    public ExploreItem(int backgroundResId, String title, String quote, String author) {
        mBackgroundResId = backgroundResId;
        mTitle = title;
        mQuote = quote;
        mAuthor = author;
    }
}
