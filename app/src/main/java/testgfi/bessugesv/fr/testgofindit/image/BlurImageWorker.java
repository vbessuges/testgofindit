package testgfi.bessugesv.fr.testgofindit.image;

import android.content.Context;
import android.graphics.Bitmap;
import android.support.v8.renderscript.Allocation;
import android.support.v8.renderscript.Element;
import android.support.v8.renderscript.RenderScript;
import android.support.v8.renderscript.ScriptIntrinsicBlur;

/**
 *
 * Worker used to create a blurred version of an image.<br/>
 * The image processing will be done in background.<br/>
 * Renderscript is used to create the blurred image.
 *
 * Created by Vincent Bessuges on 27/12/2014.
 */
public class BlurImageWorker extends ImageWorker<Bitmap> {

    public BlurImageWorker(Context context, Delegate delegate){
        super(context, delegate);
    }

    @Override
    protected Bitmap doInBackground(Bitmap... params) {
        Bitmap bmpOriginal = params[0];
        Bitmap bmpResult = Bitmap.createBitmap(bmpOriginal.getWidth(), bmpOriginal.getHeight(), bmpOriginal.getConfig());
        RenderScript rs = RenderScript.create(mContext);

        final Allocation input = Allocation.createFromBitmap(rs, bmpOriginal);
        final Allocation output = Allocation.createTyped(rs, input.getType());
        final ScriptIntrinsicBlur script = ScriptIntrinsicBlur.create(rs, Element.U8_4(rs));
        script.setRadius(25f);
        script.setInput(input);
        script.forEach(output);
        output.copyTo(bmpResult);
        return bmpResult;
    }

    @Override
    protected void onPostExecute(Bitmap bitmap) {
        if(bitmap == null) return;
        mDelegate.onWorkerDone(bitmap);
    }
}
