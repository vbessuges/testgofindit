package testgfi.bessugesv.fr.testgofindit.image;

import android.content.Context;
import android.graphics.Bitmap;
import android.widget.ImageView;

import java.lang.ref.WeakReference;

import testgfi.bessugesv.fr.testgofindit.utils.ImageHelper;

/**
 * Worker used to load an image bitmap in background, and given an imageview decode the bitmap at an optimized size for the imageview,
 * eg. the smallest possible to fill as little memory as possible, but also big enough so it won't be pixelated.<br/><br>
 *
 * It is possible to set the imageview's size divider. Changing it will alter the imageview's size, and thus will change the size used for the the bitmap.
 * See {@link #setImageSizeDivider(int)} for more info.<br/><br/>
 *
 * Created by Vincent Bessuges on 27/12/2014.
 */
public class ImageLoadingWorker extends ImageWorker<Integer> {
    private WeakReference<ImageView> mBgImg;

    private int mImageSizeDivider = 1;

    /**
     * To change the divider - an optimization parameter.<br/>
     * By default, it is set to 1. Set a higher value if you want a more pixelated - but lighter - image loaded into memory.
     * @param divider divider parameter used in the bitmap decoding computation
     */
    public void setImageSizeDivider(int divider){
        mImageSizeDivider = divider;
    }

    public ImageLoadingWorker(Context context, Delegate delegate, ImageView bgImg){
        super(context, delegate);
        mBgImg = new WeakReference<ImageView>(bgImg);
    }

    @Override
    protected Bitmap doInBackground(Integer... params) {
        if(mBgImg == null) return null;
        ImageView image = mBgImg.get();
        if(image == null || image.getWidth() == 0 || image.getHeight() == 0) return null;
        return ImageHelper.decodeSampleBitmapFromRes(
                mContext.getResources(), params[0], image.getWidth() / mImageSizeDivider, image.getHeight() / mImageSizeDivider);
    }

    @Override
    protected void onPostExecute(Bitmap bitmap) {
        if(mBgImg == null || bitmap == null) return;
        final ImageView image = mBgImg.get();
        if(image != null){
            image.setImageBitmap(bitmap);
            if(mDelegate != null) mDelegate.onWorkerDone(bitmap);
        }
    }
}
