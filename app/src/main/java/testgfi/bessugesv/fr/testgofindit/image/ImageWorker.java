package testgfi.bessugesv.fr.testgofindit.image;

import android.content.Context;
import android.graphics.Bitmap;
import android.os.AsyncTask;

/**
 * Abstract worker to process images. Provides a delegation mechanism when the processing is done on the image.<br/><br/>
 *
 * Created by Vincent Bessuges on 27/12/2014.
 */
public abstract class ImageWorker<Params> extends AsyncTask<Params, Void, Bitmap> {

    protected Context mContext;
    protected Delegate mDelegate;

    public ImageWorker(Context context, Delegate delegate){
        mContext = context;
        mDelegate = delegate;
    }

    public interface Delegate {
        public void onWorkerDone(Bitmap bitmap);
    }
}
