package testgfi.bessugesv.fr.testgofindit;

import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;

import testgfi.bessugesv.fr.testgofindit.fragments.FgtExplore;


public class AyTestGoFindIt extends ActionBarActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.ay_test_go_find_it);
        if (savedInstanceState == null) {
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.container, new FgtExplore())
                    .commit();
        }
    }
}
