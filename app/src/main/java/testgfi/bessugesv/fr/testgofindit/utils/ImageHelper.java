package testgfi.bessugesv.fr.testgofindit.utils;

import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import java.io.File;

/**
 * Utility class around image loading.
 * @author Vincent Bessuges
 *
 */
public class ImageHelper {

    /**
     * Given an image's dimensions, returns the no-more-than-necessary sample size so the image fits a specific square size.
     * @param options the object containing the image's dimensions
     * @param reqWidth the width of the specific square to fit the image in
     * @param reqHeight the height of the specific square to fit the image in
     * @return the resulting sample size
     * @see #decodeSampleBitmapFromFile(File, int, int)
     */
    public static int calculateInSampleSize(BitmapFactory.Options options, int reqWidth, int reqHeight){
        // raw image dimensions
        final int width = options.outWidth;
        final int height = options.outHeight;
        int inSampleSize = 1;

        if(width > reqWidth || height > reqHeight ){
            final int halfWidth = width / 2;
            final int halfHeight = height / 2;

            // computing the best sample size that allow the image to be correctly displayed in the required size
            // note: inSampleSize is a power of two because the decoder uses a final value by rounding down to the nearest power of two
            // see the documentation about inSampleSize
            while(halfWidth / inSampleSize > reqWidth && halfHeight / inSampleSize > reqHeight){
                inSampleSize *= 2;
            }
        }

        return inSampleSize;
    }

    /**
     * Given a file containing an image + a specific square size, will return the image loaded in a bitmap with a size corresponding to the square size argument.<br/>
     * So the image won't be too big for nothing, and not pixelated as well.
     * @param file the file containing the image
     * @param reqWidth the width of the specific square to fit the image in
     * @param reqHeight the height of the specific square to fit the image in
     * @return the loaded image in a bitmap
     */
    public static Bitmap decodeSampleBitmapFromFile(File file, int reqWidth, int reqHeight){
        final BitmapFactory.Options options = new BitmapFactory.Options();
        // to get only the image's dimensions without having to load the whole bitmap
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(file.getAbsolutePath(), options);

        // computing the insamplesize
        options.inSampleSize = calculateInSampleSize(options, reqWidth, reqHeight);

        // we're ready to get the sample
        options.inJustDecodeBounds = false;
        return BitmapFactory.decodeFile(file.getAbsolutePath(), options);
    }

    /**
     * Given the resources information of an image, will return the image loaded in a bitmap with a size corresponding to the square size argument.<br/>
     * So the image won't be too big for nothing, and not pixelated as well.
     * @param res the resources object containing the image data
     * @param resId the resource id of the image data
     * @param reqWidth the width of the specific square to fit the image in
     * @param reqHeight the height of the specific square to fit the image in
     * @return the loaded image in a bitmap
     */
    public static Bitmap decodeSampleBitmapFromRes(Resources res, int resId, int reqWidth, int reqHeight){
        final BitmapFactory.Options options = new BitmapFactory.Options();
        // to get only the image's dimensions without having to load the whole bitmap
        options.inJustDecodeBounds = true;

        BitmapFactory.decodeResource(res, resId, options);

        // computing the insamplesize
        options.inSampleSize = calculateInSampleSize(options, reqWidth, reqHeight);

        // we're ready to get the sample
        options.inJustDecodeBounds = false;
        return BitmapFactory.decodeResource(res, resId, options);
    }

}
