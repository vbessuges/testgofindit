package testgfi.bessugesv.fr.testgofindit.views;

import android.content.Context;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import testgfi.bessugesv.fr.testgofindit.R;
import testgfi.bessugesv.fr.testgofindit.image.BlurImageWorker;
import testgfi.bessugesv.fr.testgofindit.image.ImageLoadingWorker;
import testgfi.bessugesv.fr.testgofindit.image.ImageWorker;
import testgfi.bessugesv.fr.testgofindit.model.ExploreItem;
import testgfi.bessugesv.fr.testgofindit.utils.UIHelper;

/**
 * "Explore" card view. View used in the app's pager to display an explore card.<br/>
 * Handles data binding, image loading, animation, parallax effects.
 *
 * <br/><br/>Created by Vincent Bessuges on 27/12/2014.
 */
public class VExploreItem extends RelativeLayout {

    public static final int ANIMATION_DURATION = 1500;

    // DATA model item
    private ExploreItem mItem;

    // FLAGS used to handles the different async loadings states
    // this one is to make sure the image is loaded only when we know the size of the view, as we use the size as a decode parameter
    private boolean mNeedsBgBinding = false;
    // this one is to make sure the animation is not done multiple times for the same couple (view * data model item)
    private boolean mAnimationDone = false;
    // this one it to remember an animation has been requested while the blured image was still being created
    private boolean mAnimateAfterBluredImgCreation = false;
    private ImageWorker.Delegate mBlurImageDelegate = new ImageWorker.Delegate() {
        @Override
        public void onWorkerDone(Bitmap bitmap) {
            mImgBgOver.setImageBitmap(bitmap);
            if(mAnimateAfterBluredImgCreation){
                doExploreAnimation();
            }
        }
    };
    // VIEWS
    private ImageView mImgBg;
    private ImageView mImgBgOver;
    // container of all the texts
    private View mContTexts;
    private TextView mTxtTitle;
    private TextView mTxtQuote;
    private TextView mTxtAuthor;
    // WORKERS
    private ImageLoadingWorker mBgImgWorker;
    private BlurImageWorker mBlurImgWorker;
    // IMAGE WORKER DELEGATES
    private ImageWorker.Delegate mImageLoadingDelegate = new ImageWorker.Delegate() {
        @Override
        public void onWorkerDone(Bitmap bitmap) {
            createBluredBitmap(bitmap);
        }
    };


    public VExploreItem(Context context){
        this(context, null);
    }
    public VExploreItem(Context context, AttributeSet attrs){
        super(context, attrs);

        LayoutInflater.from(context).inflate(R.layout.v_explore_item, this, true);

        mImgBg = (ImageView) findViewById(R.id.img_bg);
        mImgBgOver = (ImageView) findViewById(R.id.img_bg_over);

        mContTexts = findViewById(R.id.cont_texts);

        // settings the texts fonts
        mTxtTitle = (TextView) findViewById(R.id.txt_title);
        mTxtTitle.setTypeface(UIHelper.getTypeFace(context, UIHelper.FONT_STYLE.THIN));
        ((TextView) findViewById(R.id.txt_begin)).setTypeface(UIHelper.getTypeFace(context, UIHelper.FONT_STYLE.LIGHT));
        mTxtQuote = (TextView) findViewById(R.id.txt_quote);
        mTxtQuote.setTypeface(UIHelper.getTypeFace(context, UIHelper.FONT_STYLE.REGULAR));
        mTxtAuthor = (TextView) findViewById(R.id.txt_author);
        mTxtAuthor.setTypeface(UIHelper.getTypeFace(context, UIHelper.FONT_STYLE.REGULAR));

        // will set the background images at their default position
        setBackgroundShift(0);
    }

    /**
     * Set the card's content. Will bind the data to the view and reset all flags to their right values.
     * @param item the new item to display
     */
    public void setItem(ExploreItem item){
        if(mItem != null){
            dispose();
        }
        mItem = item;
        mNeedsBgBinding = true;
        mAnimationDone = false;
        bind();
    }

    /**
     * Binds the data to the view.
     */
    private void bind() {
        bindBackground();
        mTxtTitle.setText(mItem.getTitle().toUpperCase());
        mTxtAuthor.setText(mItem.getAuthor().toUpperCase());
        mTxtQuote.setText(mItem.getQuote());
    }

    /**
     * The view is being released - its workers need to stop their job and be released as well.
     */
    private void dispose(){
        if(mBgImgWorker != null){
            mBgImgWorker.cancel(true);
            mBgImgWorker = null;
        }
        if(mBlurImgWorker != null){
            mBlurImgWorker.cancel(true);
            mBlurImgWorker = null;
        }
    }

    /**
     * Will load the image bitmap and bind it to the image views.
     * If the view has no size when this method is called (which can happen for instance at init time),
     * then the method will return and will be called later by {@link #onLayout(boolean, int, int, int, int)}.
     */
    private void bindBackground() {
        // we check that the view has a size - if not this method will be called again in onLayout
        if(getHeight() == 0 || getWidth() == 0 || !mNeedsBgBinding) return;
        mNeedsBgBinding = false;
        mBgImgWorker = new ImageLoadingWorker(getContext(), mImageLoadingDelegate, mImgBg);
        // as the image should immediately fade into a blurred version, no need to load a large bitmap in memory
        // so we set the image divider to 4
        mBgImgWorker.setImageSizeDivider(4);
        mBgImgWorker.execute(mItem.getBackgroundResId());
    }

    @Override
    protected void onLayout(boolean changed, int l, int t, int r, int b) {
        // workaround to know when the view will have a size
        super.onLayout(changed, l, t, r, b);
        if(mNeedsBgBinding) bindBackground();
    }

    /**
     * Will create and start the blurimageworkeron a bitmap.
     * @param bitmap the bitmap to be blurred
     */
    private void createBluredBitmap(Bitmap bitmap){
        if(mBlurImgWorker != null) return;
        mBlurImgWorker = new BlurImageWorker(getContext(), mBlurImageDelegate);
        mBlurImgWorker.execute(bitmap);
    }

    /**
     * Call this to start the animation. Will do nothing if the animation has already been done for the current data model item.<br/>
     * An explicit call to {@link #resetExploreAnimation()} is needed in order to restart the animation.
     */
    public void startExploreAnimation() {
        if(mAnimationDone) return;
        if(mBlurImgWorker == null || mBlurImgWorker.getStatus() != AsyncTask.Status.FINISHED){
            mAnimateAfterBluredImgCreation = true;
            return;
        }
        doExploreAnimation();
    }

    /**
     * Call this to reset the animation's flag and views to their original states.<br/>
     * A new call to {@link #startExploreAnimation()} can then be called to do the animation again.
     */
    public void resetExploreAnimation() {
        mAnimationDone = false;
        mImgBg.setVisibility(View.VISIBLE);
        mImgBgOver.setVisibility(View.GONE);
        mContTexts.setVisibility(View.INVISIBLE);
    }

    /**
     * Actual animation code.
     */
    private void doExploreAnimation(){
        if(mAnimationDone) return;
        mAnimationDone = true;

        // IMAGE ANIMATION SETUP - FADE IN OF THE BLURRED IMAGE OVER THE ORIGINAL ONE
        AlphaAnimation imageFadeIn = new AlphaAnimation(0f, 1f);
        imageFadeIn.setDuration(ANIMATION_DURATION);
        imageFadeIn.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
                mImgBgOver.setVisibility(View.VISIBLE);
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                mImgBg.setVisibility(View.GONE);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {}
        });


        // TEXTS ANIMATION SETUP - FADE IN - this animation is in two phases
        // - the first phase will take 2/3rd of the animation time to change the opacity from 0 to 50%
        // - the second phase will take the remaining 1/3rd of the animation time to change the opacity from 50 to 100%
        // by doing this we ensure the text don't become bright too quickly
        AlphaAnimation textsFadeIn1 = new AlphaAnimation(0f, 0.5f);
        textsFadeIn1.setDuration(2 * ANIMATION_DURATION / 3);
        final AlphaAnimation textsFadeIn2 = new AlphaAnimation(0.5f, 1f);
        textsFadeIn2.setDuration(ANIMATION_DURATION / 3);
        textsFadeIn1.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
                mContTexts.setVisibility(View.VISIBLE);
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                // starting the second phase of the animation
                mContTexts.startAnimation(textsFadeIn2);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {}
        });


        // this is to make sure the animation will start, because on first rendering, a "gone" view is considered as inactive
        // and thus when the app starts if the user does not interact with the screen the animation will never start
        mImgBgOver.setVisibility(View.INVISIBLE);

        mImgBgOver.startAnimation(imageFadeIn);
        mContTexts.startAnimation(textsFadeIn1);
    }

    /**
     * Parallax effect on the background.
     * @param position between -1 and 1, 0 being the normal position, -1 the farthest left and 1 the farthest right.
     */
    public void setBackgroundShift(float position){
        int width = getWidth();
        // magic formula of the parallax effect
        float left =  -(width*1.1f * position) -0.1f * width;
        float right = (width*1.1f * position) -0.1f * width ;

        // changing the margin of both images to the computed values above
        FrameLayout.LayoutParams bgLp = (FrameLayout.LayoutParams) mImgBg.getLayoutParams();
        FrameLayout.LayoutParams bgOverLp = (FrameLayout.LayoutParams) mImgBgOver.getLayoutParams();

        bgLp.rightMargin = bgOverLp.rightMargin = (int)right;
        bgLp.leftMargin = bgOverLp.leftMargin = (int)left;

        mImgBg.setLayoutParams(bgLp);
        mImgBgOver.setLayoutParams(bgOverLp);
    }
}
