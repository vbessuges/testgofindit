package testgfi.bessugesv.fr.testgofindit.fragments;

import android.content.res.Resources;
import android.content.res.TypedArray;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import java.util.ArrayList;

import testgfi.bessugesv.fr.testgofindit.R;
import testgfi.bessugesv.fr.testgofindit.model.ExploreItem;
import testgfi.bessugesv.fr.testgofindit.utils.UIHelper;
import testgfi.bessugesv.fr.testgofindit.views.VExploreItem;

/**
 *
 * Fragment used to display "Explore" cards.
 *
 * Created by Vincent Bessuges on 27/12/2014.
 */
public class FgtExplore extends Fragment {
    private static final int ARROW_ALPHA_VALUE = 95;

    private ExploreAdapter mAdapter;
    private ViewPager mPager;
    // arrows to scroll through pages
    private ImageView mImgLeft, mImgRight;
    private int mCurrentPosition;

    private ViewPager.OnPageChangeListener mOnPageChangeListener = new ViewPager.OnPageChangeListener(){

        @Override
        public void onPageScrolled(int i, float v, int i2) { }

        @Override
        public void onPageSelected(int i) {
            updateArrowsFromPagerPosition(i);
        }

        @Override
        public void onPageScrollStateChanged(int i) {
            // waiting for the pager to settle before validating the new position
            if(i == ViewPager.SCROLL_STATE_IDLE){
                // if the position changed, we will
                // - reset the animation on the previous page (if any)
                // - start the animation on the new page
                if(mCurrentPosition != mPager.getCurrentItem()){
                    VExploreItem vItem = mAdapter.getViewAt(mCurrentPosition);
                    if(vItem != null) vItem.resetExploreAnimation();
                    mCurrentPosition = mPager.getCurrentItem();
                    vItem = mAdapter.getViewAt(mCurrentPosition);
                    if(vItem != null) vItem.startExploreAnimation();
                }
            }
        }
    };


    // empty constructor as this is a fragment class
    public FgtExplore() {}

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fgt_explore, container, false);

        // ARROWS SETUP
        mImgLeft = (ImageView) view.findViewById(R.id.img_left);
        UIHelper.setImageViewAlpha(mImgLeft, ARROW_ALPHA_VALUE);
        mImgLeft.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                mPager.setCurrentItem(mCurrentPosition-1);
            }
        });
        mImgRight = (ImageView) view.findViewById(R.id.img_right);
        UIHelper.setImageViewAlpha(mImgRight, ARROW_ALPHA_VALUE);
        mImgRight.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                mPager.setCurrentItem(mCurrentPosition+1);
            }
        });

        // PAGER AND ADAPTER INITIALIZATION
        mPager = (ViewPager) view.findViewById(R.id.pager);
        mAdapter = new ExploreAdapter();

        // building the adapter's list
        ArrayList<ExploreItem> items = new ArrayList<ExploreItem>();
        Resources res = getResources();
        TypedArray images = res.obtainTypedArray(R.array.images);
        TypedArray titles = res.obtainTypedArray(R.array.titles);
        TypedArray quotes = res.obtainTypedArray(R.array.quotes);
        TypedArray authors = res.obtainTypedArray(R.array.authors);
        for(int i = 0 ; i < images.length() ; i++){
            items.add(new ExploreItem(images.getResourceId(i, 0), titles.getString(i), quotes.getString(i), authors.getString(i)));
        }
        mAdapter.setItems(items);

        // current position setup
        mCurrentPosition = mPager.getCurrentItem();
        // to make sure the animation will start also on the first displayed item
        mAdapter.setInitialPosition(mCurrentPosition);
        updateArrowsFromPagerPosition(mCurrentPosition);

        mPager.setAdapter(mAdapter);
        mPager.setOnPageChangeListener(mOnPageChangeListener);
        mPager.setPageTransformer(true, new ExplorerTransformer());

        return view;
    }

    /**
     * Will change the arrows visibility depending on the current page:
     * <ul>
     *     <li>if on the first page, hide the left arrow</li>
     *     <li>if on the last page, hide the right arrow</li>
     * </ul>
     * @param page the current page
     */
    private void updateArrowsFromPagerPosition(int page){
        mImgLeft.setVisibility(page == 0 ? View.GONE : View.VISIBLE);
        mImgRight.setVisibility(page == mAdapter.getCount() - 1 ? View.GONE : View.VISIBLE);
    }

    /**
     * Adapter used to manage the card views. Can be used to retrieve a card's view according to its position.
     */
    class ExploreAdapter extends PagerAdapter {
        private int mInitialPosition = -1;
        private ArrayList<ExploreItem> mItems;
        private SparseArray<VExploreItem> mViewsByPosition = new SparseArray<VExploreItem>();

        public void setInitialPosition(int position){
            mInitialPosition = position;
        }

        public void setItems(ArrayList<ExploreItem> items){
            mItems = new ArrayList<ExploreItem>(items);
        }

        public VExploreItem getViewAt(int position){
            return mViewsByPosition.get(position);
        }

        @Override
        public int getCount() {
            return mItems == null ? 0 : mItems.size();
        }

        @Override
        public boolean isViewFromObject(View view, Object o) {
            return view == o;
        }

        @Override
        public Object instantiateItem(ViewGroup container, int position) {
            VExploreItem vItem = new VExploreItem(container.getContext());
            mViewsByPosition.put(position, vItem);
            container.addView(vItem);
            vItem.setItem(mItems.get(position));
            // we make sure here that the first view is animated when the pager is initialized
            if(mInitialPosition == position){
                vItem.startExploreAnimation();
                mInitialPosition = -1;
            }
            return vItem;
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            mViewsByPosition.remove(position);
            container.removeView((View) object);
        }
    }

    /**
     * Page animation - we don't alternate the standard movement of the page provided by the Android ViewPager,
     * but we give the current position shift to the card views so they can use it to add a parallax effect on their
     * backgrounds.
     */
    class ExplorerTransformer implements ViewPager.PageTransformer {
        @Override
        public void transformPage(View page, float position) {
            // for every page currently in movement, the position is given to the view
            ((VExploreItem)page).setBackgroundShift(position);
        }
    }
}