package testgfi.bessugesv.fr.testgofindit.utils;

import android.content.Context;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Handler;
import android.os.Looper;
import android.widget.ImageView;

import java.util.HashMap;

/**
 * Utility class around Android-specific UI stuff.
 * @author Vincent Bessuges
 */
public class UIHelper {

    public static void runOnMainThread(Runnable task){
        Looper main = Looper.getMainLooper();
        if(Looper.myLooper() == main){
            task.run();
        }
        else {
            new Handler(main).post(task);
        }
    }

    /**
     * Utility method used to change the alpha of an image. Uses different platform methods, depending on the API level of the device.
     * @param img
     * @param alpha
     */
    @SuppressWarnings("deprecation")
    public static void setImageViewAlpha(ImageView img, int alpha){
        if(Build.VERSION.SDK_INT >= 16){
            img.setImageAlpha(alpha);
        }
        else {
            img.setAlpha(alpha);
        }
    }


    /* FONTS */

    public enum FONT_STYLE {REGULAR, THIN, LIGHT};
    private static HashMap<String, Typeface> typeFaces = new HashMap<String, Typeface>();

    private static String getTypeFaceName(FONT_STYLE fontStyle){
        switch(fontStyle){
            case LIGHT:     return "YanoneKaffeesatz_Light.ttf";
            case THIN:      return "YanoneKaffeesatz_Thin.ttf";
            default:        return "YanoneKaffeesatz_Regular.ttf";
        }
    }

    /**
     * Utility method to get the custom fonts used in the app.
     * Those fonts are loaded from the app's resources if used for the first time, and are cached for later uses.
     * @param context used to load resources if need be
     * @param style either {@link FONT_STYLE#REGULAR}, {@link FONT_STYLE#THIN} or {@link FONT_STYLE#LIGHT}
     * @return the font ready to be used in a textview
     */
    public static Typeface getTypeFace(Context context, FONT_STYLE style) {
        String typeFaceName = getTypeFaceName(style);
        if(!typeFaces.containsKey(typeFaceName)){
            typeFaces.put(typeFaceName, Typeface.createFromAsset(context.getAssets(), typeFaceName));
        }
        return typeFaces.get(typeFaceName);
    }
}
